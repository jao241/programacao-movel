const CADASTRO = {
    descricao: "cadastro de pessoas",
    pessoas: [
        {
            nome: "joão da silva",
            idade: 25,
            profissao: "médico",
        },
        {
            nome: "josé da silva",
            idade: 21,
            profissao: "engenheiro",
        },
        {
            nome: "maria da silva",
            idade: 20,
            profissao: "estudante",
        }
    ]
}

imprimirDadosPessoais = (cadastro) => {
    const pessoas = cadastro.pessoas;

    let count = 1;
    for (const pessoa of pessoas) {
        console.log("Pessoa #" + count + ": " + pessoa.nome + ", idade = " + 
            pessoa.idade + ", profissão = " + pessoa.profissao);

        count++;
    }

}

imprimirDadosPessoais(CADASTRO);